package com.example.raam.submission2v02.model

import com.google.gson.annotations.SerializedName

data class TeamResponse(
        @SerializedName("teams")
        var teams: List<Team>
)