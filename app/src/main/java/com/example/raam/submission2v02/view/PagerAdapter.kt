package com.example.raam.submission2v02.view

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class PagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    private val listFragment = ArrayList<Fragment>()
    private val titleList = ArrayList<String>()

    override fun getItem(p0: Int): Fragment {
        return listFragment[p0]
    }

    override fun getCount(): Int = listFragment.size

    override fun getPageTitle(position: Int): CharSequence? {
        super.getPageTitle(position)
        return titleList[position]
    }

    fun addingFragment(fragment: Fragment, title: String) {
        listFragment.add(fragment)
        titleList.add(title)
    }
}