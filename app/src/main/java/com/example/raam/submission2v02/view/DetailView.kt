package com.example.raam.submission2v02.view

import com.example.raam.submission2v02.model.EventDetail
import com.example.raam.submission2v02.model.Team

interface DetailView {
    fun showEventList(event: List<EventDetail>)
    fun homeTeam(data: List<Team>)
    fun awayTeam(data: List<Team>)
}