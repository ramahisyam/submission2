package com.example.raam.submission2v02.view.detailmatch

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.raam.submission2v02.R
import com.example.raam.submission2v02.api.ApiRepository
import com.example.raam.submission2v02.model.EventDetail
import com.example.raam.submission2v02.model.Team
import com.example.raam.submission2v02.presenter.DetailMatchPresenter
import com.example.raam.submission2v02.view.DetailView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail.*
import java.text.SimpleDateFormat
import java.util.*

class DetailActivity : AppCompatActivity(), DetailView {

    private lateinit var presenter: DetailMatchPresenter

    companion object {
        val EXTRA = "extra"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        presenter = DetailMatchPresenter(this, ApiRepository(), Gson())
        presenter.getDetailMatch(intent.getStringExtra("extra"))

    }

    override fun showEventList(event: List<EventDetail>) {

        presenter.getHomeTeam(event[0].idHomeTeam)
        presenter.getAwayteam(event[0].idAwayTeam)

        val date = SimpleDateFormat("EEE, dd MM yyyy", Locale.getDefault())
        detail_date.text = date.format(event[0].dateEvent)
        detail_league.text = event[0].strLeague
        detail_home_team.text = event[0].strHomeTeam
        detail_away_team.text = event[0].strAwayTeam
        detail_score_home.text = event[0].intHomeScore
        detail_score_away.text = event[0].intAwayScore
        detail_goal_home.text = event[0].strHomeGoalDetails
        detail_goal_away.text = event[0].strAwayGoalDetails
        detail_shots_home.text = event[0].intHomeShots
        detail_shots_away.text = event[0].intAwayShots
        detail_goalkeeper_home.text = event[0].strHomeLineupGoalkeeper
        detail_goalkeeper_away.text = event[0].strAwayLineupGoalkeeper
        detail_defense_home.text = event[0].strHomeLineupDefense
        detail_defense_away.text = event[0].strAwayLineupDefense
        detail_midfield_home.text = event[0].strHomeLineupMidfield
        detail_midfield_away.text = event[0].strAwayLineupMidfield
        detail_forward_home.text = event[0].strAwayLineupForward
        detail_forward_away.text = event[0].strHomeLineupForward
        detail_substitutes_home.text = event[0].strHomeLineupSubstitutes
        detail_substitutes_away.text = event[0].strAwayLineupSubstitutes

    }

    override fun homeTeam(data: List<Team>) {
        Glide.with(this).load(data[0].strTeamBadge).into(detail_img_home)
    }

    override fun awayTeam(data: List<Team>) {
        Glide.with(this).load(data[0].strTeamBadge).into(detail_img_away)
    }
}
