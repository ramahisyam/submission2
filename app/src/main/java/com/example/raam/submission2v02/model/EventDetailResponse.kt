package com.example.raam.submission2v02.model

import com.google.gson.annotations.SerializedName

class EventDetailResponse(
        @SerializedName("events")
        val events: List<EventDetail>
)