package com.example.raam.submission2v02.view.nextmatch

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.raam.submission2v02.model.Event
import kotlinx.android.synthetic.main.match_row.view.*
import java.text.SimpleDateFormat
import java.util.*

class NextMatchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bind(events: Event, listener: (Event) -> Unit) {
        val formatDate = SimpleDateFormat("EEE, dd MMM yyy", Locale.getDefault())

        itemView.match_date_event.text = formatDate.format(events.dateEvent)
        itemView.match_home_team.text = events.strHomeTeam
        itemView.match_away_team.text = events.strAwayTeam

        itemView.setOnClickListener {
            listener(events)
        }
    }
}