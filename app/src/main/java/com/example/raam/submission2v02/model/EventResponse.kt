package com.example.raam.submission2v02.model

import com.google.gson.annotations.SerializedName

data class EventResponse(
        @SerializedName("events")
        val events: List<Event>
)