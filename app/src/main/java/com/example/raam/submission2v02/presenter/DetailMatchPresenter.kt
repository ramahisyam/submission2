package com.example.raam.submission2v02.presenter

import com.example.raam.submission2v02.api.ApiRepository
import com.example.raam.submission2v02.api.SportDBApi
import com.example.raam.submission2v02.model.EventDetailResponse
import com.example.raam.submission2v02.model.TeamResponse
import com.example.raam.submission2v02.view.DetailView
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DetailMatchPresenter(private val view: DetailView,
                           private val apiRepository: ApiRepository,
                           private val gson: Gson) {
    fun getDetailMatch(matchId: String?)  {
        doAsync {
            val data = gson.fromJson(apiRepository.
                    doRequest(SportDBApi.getMatchDetail(matchId)), EventDetailResponse::class.java)
            uiThread {
                view.showEventList(data.events)
            }
        }
    }

    fun getHomeTeam(teamId: String?)  {
        doAsync {
            val data = gson.fromJson(apiRepository.
                    doRequest(SportDBApi.getTeam(teamId)), TeamResponse::class.java)
            uiThread {
                view.homeTeam(data.teams)
            }
        }

    }

    fun getAwayteam(teamId: String?) {
        doAsync {
            val data = gson.fromJson(apiRepository.
                    doRequest(SportDBApi.getTeam(teamId)), TeamResponse::class.java)
            uiThread {
                view.awayTeam(data.teams)
            }
        }
    }
}