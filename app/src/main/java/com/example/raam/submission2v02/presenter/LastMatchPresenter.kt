package com.example.raam.submission2v02.presenter

import com.example.raam.submission2v02.api.ApiRepository
import com.example.raam.submission2v02.api.SportDBApi
import com.example.raam.submission2v02.model.EventResponse
import com.example.raam.submission2v02.view.MainView
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class LastMatchPresenter(private val view: MainView,
                         private val apiRepository: ApiRepository,
                         private val gson: Gson) {
    fun getLastMatch() {
        view.showLoading()
        doAsync {
            val data = gson.fromJson(apiRepository.
                    doRequest(SportDBApi.getLastMatch()), EventResponse::class.java)
            uiThread {
                view.hideLoading()
                view.showEventList(data.events)
            }
        }
    }
}