package com.example.raam.submission2v02.view.nextmatch

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.raam.submission2v02.R
import com.example.raam.submission2v02.model.Event

class NextMatchAdapter(private val eventList: List<Event>, private val listener: (Event) -> Unit) :
        RecyclerView.Adapter<NextMatchViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NextMatchViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.match_row, p0, false)

        return NextMatchViewHolder(view)
    }

    override fun getItemCount(): Int = eventList.size

    override fun onBindViewHolder(p0: NextMatchViewHolder, p1: Int) {
        p0.bind(eventList[p1], listener)
    }
}
