package com.example.raam.submission2v02.model

import com.google.gson.annotations.SerializedName

data class Team(
        @SerializedName("idTeam")
        var idTeam: String? = null,

        @SerializedName("strTeamBadge")
        var strTeamBadge: String? = null
)