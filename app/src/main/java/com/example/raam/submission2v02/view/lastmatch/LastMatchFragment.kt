package com.example.raam.submission2v02.view.lastmatch


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.raam.submission2v02.R
import com.example.raam.submission2v02.api.ApiRepository
import com.example.raam.submission2v02.model.Event
import com.example.raam.submission2v02.presenter.LastMatchPresenter
import com.example.raam.submission2v02.util.invisible
import com.example.raam.submission2v02.util.visible
import com.example.raam.submission2v02.view.MainView
import com.example.raam.submission2v02.view.detailmatch.DetailActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_last_match.*
import org.jetbrains.anko.support.v4.startActivity


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class LastMatchFragment : Fragment(), MainView {

    private var events: MutableList<Event> = mutableListOf()
    private lateinit var mAdapter: LastMatchAdapter
    private lateinit var presenter: LastMatchPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_last_match, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = LastMatchPresenter(this, ApiRepository(), Gson())
        presenter.getLastMatch()

        mAdapter = LastMatchAdapter(events) {
            startActivity<DetailActivity>(DetailActivity.EXTRA to it.idEvent)
        }

        list_last_match.layoutManager = LinearLayoutManager(activity)
        list_last_match.adapter = mAdapter
    }

    override fun showLoading() {
        progress_loading.visible()
    }

    override fun hideLoading() {
        progress_loading.invisible()
    }

    override fun showEventList(event: List<Event>) {
        events.clear()
        events.addAll(event)
        mAdapter.notifyDataSetChanged()
    }

}
