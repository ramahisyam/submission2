package com.example.raam.submission2v02.view

import com.example.raam.submission2v02.model.Event

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showEventList(event: List<Event>)
}