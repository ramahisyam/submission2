package com.example.raam.submission2v02.view

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import com.example.raam.submission2v02.R
import com.example.raam.submission2v02.view.lastmatch.LastMatchFragment
import com.example.raam.submission2v02.view.nextmatch.NextMatchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var viewPager: ViewPager? = null
    private var tabsMain: TabLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar?.title = "Schedule"

        viewPager = view_pager as ViewPager
        setViewPager(viewPager!!)

        tabsMain = tabs_main as TabLayout
        tabsMain!!.setupWithViewPager(viewPager)

    }
    fun setViewPager(viewPager: ViewPager) {
        val mAdapter = PagerAdapter(supportFragmentManager)
        mAdapter.addingFragment(NextMatchFragment(), "Next Match")
        mAdapter.addingFragment(LastMatchFragment(), "Last Match")
        viewPager.adapter = mAdapter
    }
}
